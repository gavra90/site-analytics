<?php

declare(strict_types=1);

namespace SiteAnalytics\Domain;

class AnalyticsId
{
    /**
     * @var int
     */
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->id;
    }
}
