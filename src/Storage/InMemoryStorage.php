<?php

declare(strict_types=1);

namespace SiteAnalytics\Storage;

class InMemoryStorage implements Persistence
{
    private $data = [
        1 => [
            'site' => 'facebook',
            'visits' => 1500,
            'month' => 1
        ],
        2 => [
            'site' => 'youtube',
            'visits' => 2500,
            'month' => 2
        ],
    ];
    public function getAll()
    {
        return $this->data;
    }

    public function get(int $id)
    {
        return $this->data[$id];
    }
}
