<?php

declare(strict_types=1);

namespace SiteAnalytics\Storage;

use SiteAnalytics\Domain\AnalyticsId;

class AnalitycsRepository
{
    /**
     * @var Persistence
     */
    private $persistence;

    /**
     * @param Persistence $persistence
     */
    public function __construct(Persistence $persistence)
    {
        $this->persistence = $persistence;
    }

    public function getById(AnalyticsId $analyticsId)
    {
        return $this->persistence->get($analyticsId->getValue());
    }

    public function getAll()
    {
        return $this->persistence->getAll();
    }
}
