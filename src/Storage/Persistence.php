<?php

declare(strict_types=1);

namespace SiteAnalytics\Storage;

interface Persistence
{
    public function getAll();
    public function get(int $id);
}
