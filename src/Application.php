<?php

declare(strict_types=1);

namespace SiteAnalytics;

use FastRoute\Dispatcher;
use Pimple\Container;
use SiteAnalytics\Controller\RequestHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Application
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;
    /**
     * @var Container
     */
    private $container;

    public function __construct(Dispatcher $dispatcher, Container $container)
    {
        $this->dispatcher = $dispatcher;
        $this->container = $container;
    }

    /**
     * @throws \JsonException
     */
    public function run(Request $request, Response $response)
    {
        $routeInfo = $this->dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
        if (Dispatcher::FOUND === $routeInfo[0]) {
            $handlerFactory = $routeInfo[1];
            /** @var RequestHandlerInterface $handler */
            $handler = (new $handlerFactory())($this->container);
            /** @var Response $response */
            $response = $handler($request, $response);
        }
        if (Dispatcher::NOT_FOUND === $routeInfo[0]) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND)->setContent(json_encode([
                'error' => 'Route not found'
            ], JSON_THROW_ON_ERROR));
        }
        if (Dispatcher::METHOD_NOT_ALLOWED === $routeInfo[0]) {
            $response->setStatusCode(Response::HTTP_METHOD_NOT_ALLOWED)->setContent(json_encode([
                'error' => 'Method not allowed'
            ], JSON_THROW_ON_ERROR));
        }
        $response->headers->set('Content-Type', 'application/json');
        $response->prepare($request);
        $response->send();
    }
}
