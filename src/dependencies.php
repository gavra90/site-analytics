<?php

use Pimple\Container;
use SiteAnalytics\Storage\AnalitycsRepository;
use SiteAnalytics\Storage\InMemoryStorage;

$container = new Container();
$container[InMemoryStorage::class] = static function () {
    return  new InMemoryStorage();
};
$container[AnalitycsRepository::class] = static function () use ($container) {
    return new AnalitycsRepository($container->raw(InMemoryStorage::class)());
};
