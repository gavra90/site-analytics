<?php

declare(strict_types=1);

namespace SiteAnalytics\Controller\Factory;

use Pimple\Container;
use SiteAnalytics\Controller\RequestHandlerInterface;

interface ControllerFactoryInterface
{
    public function __invoke(Container $container): RequestHandlerInterface;
}
