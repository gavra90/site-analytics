<?php

declare(strict_types=1);

namespace SiteAnalytics\Controller\Factory;

use Pimple\Container;
use SiteAnalytics\Controller\AnalyticsGetAllController;
use SiteAnalytics\Controller\RequestHandlerInterface;
use SiteAnalytics\Storage\AnalitycsRepository;

class AnalyticsGetAllControllerFactory implements ControllerFactoryInterface
{
    public function __invoke(Container $container): RequestHandlerInterface
    {
        return new AnalyticsGetAllController($container->raw(AnalitycsRepository::class)());
    }
}
