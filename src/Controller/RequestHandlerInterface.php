<?php

declare(strict_types=1);

namespace SiteAnalytics\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface RequestHandlerInterface
{
    public function __invoke(Request $request, Response $response): Response;
}
