<?php

declare(strict_types=1);

namespace SiteAnalytics\Controller;

use SiteAnalytics\Storage\AnalitycsRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AnalyticsGetController implements RequestHandlerInterface
{
    /**
     * @var AnalitycsRepository
     */
    private $analitycsRepository;

    /**
     * @param AnalitycsRepository $analitycsRepository
     */
    public function __construct(AnalitycsRepository $analitycsRepository)
    {
        $this->analitycsRepository = $analitycsRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        return new JsonResponse([
            'data' =>$this->analitycsRepository->getById()
        ]);
    }
}
