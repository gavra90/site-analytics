<?php

use FastRoute\RouteCollector;
use SiteAnalytics\Application;
use SiteAnalytics\Controller\Factory\AnalyticsGetAllControllerFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Run;
use function FastRoute\simpleDispatcher;

require '../vendor/autoload.php';
require '../src/dependencies.php';
$develop = true; //check env
$whoops = new Run();
$whoops->pushHandler(new JsonResponseHandler());
$whoops->writeToOutput($develop); //else add some generic error and push different handler
$whoops->register();

$dispatcher = simpleDispatcher(static function (RouteCollector $collector) {
 $collector->get('/analytics', AnalyticsGetAllControllerFactory::class);
});
$application = new Application($dispatcher, $container);

$application->run(Request::createFromGlobals(), new Response());